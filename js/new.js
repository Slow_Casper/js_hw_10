
const tabs = document.querySelectorAll(".tabs_title");
const descriptions = document.querySelectorAll(".tabs_description");

function tabClickHandler(event) {
    tabs.forEach(element => {
        element.classList.remove("active");
    });
    descriptions.forEach(element => {
        element.classList.remove("active");
    });

    event.target.classList.add("active");

    const descriptionsID = event.target.getAttribute("data-tab-id");
    document.getElementById(descriptionsID).classList.add("active");
}

tabs.forEach(element => {
    element.addEventListener("click", tabClickHandler)
})
